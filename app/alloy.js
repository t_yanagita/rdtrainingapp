// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};
Ti.include('js/CommonUtil.js');

// contentの高さを調整
function resizeContentHeight() {
	var displayHeight = Ti.Platform.displayCaps.platformHeight;
	var children = this.getChildren();
	var headerHeight = children[0].getSize().height;
	var contentView = children[1];
	
	if (OS_IOS) {
		var header = this.getChildren()[0];
		header.setHeight('50dp');
		headerHeight = 50;
	}
	
	var contentHeight = displayHeight - headerHeight;
	contentView.setHeight(contentHeight);
}
