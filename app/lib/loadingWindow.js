/** アクティブインジケータ */
var actInd;
/** タブグループ */
var tabGroup;
 
/**
 * タブグループ初期化
 * @param {TabGroup} setTabGroup
 * @param {String}	setLoadingStr
 */
exports.initTabGroup = function(setTabGroup,setLoadingStr){
	actInd = Ti.UI.createActivityIndicator({
      backgroundColor:'black',
      opacity:0.7,
      height: '100%',
      width: '100%',
      font: {fontSize:14, fontWeight:'bold'}
    });
    if(Ti.Platform.osname=="android"){
    	actInd.setMessage(setLoadingStr);
    }
    if(Ti.Platform.osname=="iphone"){
    	actInd.setStyle(Titanium.UI.iPhone.ActivityIndicatorStyle.BIG);
    }
    tabGroup = setTabGroup;
    tabGroup.add(actInd);
};
 
/**
 * ローディング画面表示
 */
exports.show = function(){
	actInd.show();
};
 
/**
 * ローディング画面非表示
 */
exports.hide = function(){
	actInd.hide();
};