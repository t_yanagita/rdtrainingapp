
//ローディング画面初期化
var LoadingWindow = require('loadingWindow');

// 回答画面へ画面遷移
function forwardQuestion() {
	var view = Alloy.createController('question').getView();
	view.open();
}

var VERSION_TITLE = '現在のVer : ';

// 研修問題をロード
function loadQuestion() {
	var alertDialog = Titanium.UI.createAlertDialog({
		title : '確認',
		message : '研修データをロードしますか？',
		buttonNames : ['ロード', 'とじる'],
		cancel : 1
	});
	alertDialog.addEventListener('click', function(event) {
		// Cancelボタンが押されたかどうか
		if (event.cancel) {
			// cancel時の処理
		}
		// 選択されたボタンのindexも返る
		if (event.index == 0) {
			var content = $.content;
			content.setLayout('none');
			LoadingWindow.initTabGroup($.content, "Now Loading");
			LoadingWindow.show();

			// ロード終了処理
			var date = new Date();
			var start = date.getTime();
			// javascriptのSleepがTitaniumのバグで使用できない？
			while ((new Date().getTime() - start) < 3000) {
			}
			
			var str = VERSION_TITLE + 'v' + date.getDate() + date.getHours() + date.getMilliseconds();

			var endDialog = Titanium.UI.createAlertDialog({
				title : '完了',
				message : '研修データのロードが完了しました。\n' + str,
				buttonNames : ['とじる'],
				cancel : 0
			}).show();
			
			
			LoadingWindow.hide();
			$.loadVersion.setText(str);
		}
	});
	alertDialog.show();
}

$.index.open(); 