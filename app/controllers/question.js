// 変数
var selectAnswerNum = -1;


function selectAnswer() {
	var parent = this.getParent();
	var children = parent.getChildren();
	for (var i = 0; i < children.length; i++) {
		children[i].setBackgroundColor('#FFF');
		if (this === children[i]) {
			selectAnswerNum = i;
			this.setBackgroundColor('#FFFF54');
		}
	}
}

function sendAnswer() {
	var view = Alloy.createController('answer', {
		answerNum : selectAnswerNum
	}).getView();
	view.open();
}

function forwardTop() {
		var alertDialog = Titanium.UI.createAlertDialog({
		title : '確認',
		message : 'Topページに戻りますか？',
		buttonNames : ['もどる', 'とじる'],
		cancel : 1
	});
	alertDialog.addEventListener('click', function(event) {
		// Cancelボタンが押されたかどうか
		if (event.cancel) {
			// cancel時の処理
		}
		// 選択されたボタンのindexも返る
		if (event.index == 0) {
			var view = Alloy.createController('index').getView();
			view.open();
		}
	});
	alertDialog.show();
}
//$.question.open();