var args = arguments[0] || {};

// 選択された答えと回答の色をつける
var answerNum = args.answerNum;
var parentView = $.answerArea;
parentView.getChildren()[answerNum].setBackgroundColor('#FFFF54');

var dialogMsg = '不正解';

if (answerNum == 1) {
	dialogMsg = '正解';	
}

var answerDialog = Titanium.UI.createAlertDialog({
	title : '回答結果',
	message : dialogMsg,
	buttonNames : ['解説', '次の問題へ'],
	cancel : 0
});
answerDialog.addEventListener('click', function(event) {
	// Cancelボタンが押されたかどうか
	if (event.cancel) {
		// cancel時の処理
	}
	// 選択されたボタンのindexも返る
	if (event.index == 1) {
		var view = Alloy.createController('question').getView();
		view.open();
	}
});
answerDialog.show();

function nextQuestion() {
	var view = Alloy.createController('question').getView();
	view.open();
}

function forwardTop() {
	var alertDialog = Titanium.UI.createAlertDialog({
		title : '確認',
		message : 'Topページに戻りますか？',
		buttonNames : ['もどる', 'とじる'],
		cancel : 1
	});
	alertDialog.addEventListener('click', function(event) {
		// Cancelボタンが押されたかどうか
		if (event.cancel) {
			// cancel時の処理
		}
		// 選択されたボタンのindexも返る
		if (event.index == 0) {
			var view = Alloy.createController('index').getView();
			view.open();
		}
	});
	alertDialog.show();
}